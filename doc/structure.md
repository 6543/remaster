## 1.1 Load base functions                                      [-> #functions]

## 1.2 Check ...
  * for updates
  * for rights

## 1.3 Load settings                                             [-> #config]
  * lade /etc/remaster/<Conf>
  * verify config (Proj Exist; Mods Exist; ...)

## 1.4 Load Proj-Func                                            [-> #projects]
  * overload based on dependencys
  * (z.B.  ubuntu.16.04 -> ubuntu -> debian)
  * verify config (Proj Conform)

## 1.5 Load $n Mods (optional)                                   [-> #mods]
  * check
  * save in MOD_LST

# 2. Init Chroot

## 2.1 Extrackt Files
  * ISO
  * Squashfs

## 2.2 Config for Chroot (chroot_initial)
  * (lxc-conf / tmpdir)
  * network

## 2.3 Start Chroot
  * (lxc-start / mount ...)

# 3. Modivikationen

## 3.1 Network

## 3.2 Proj-Spez.
  * (z.B. Desinfect: conky_info)

## 3.3 Packet Mgr
  * Updates
  * Install
  * Delete

## 3.4 Next in $MOD_LST
  * z.B. xrdp
  * z.B. default pw

# 4. Finish

## 4.1 Clean-Up Live-System
  * tmpfiles
  * Packet Mgr

## 4.2 Stop chroot
  * Umount

## 4.3. Generate ISO/PXE

## 4.4. Delete Chroot

# 5. Send Log

----

## lxc

-> chroot_sh exec lxc-attach

-> chroot_dir = container name

----

## PXE

-> Install
  * DHCP Relay
  * NFS-Server
  * TFTP-Server
  * [Samba]

-> Setup
  * (use <Lib>/proj/<proj> func)
  * SMB/NFS LiveSys export
  * TFTP (Boot Menue; Boot Loader; Kernel&Initrd)

-> Deinstall
  * RM TFTP-Root
  * RM SMB/NFS LiveSys export
  * del DHCP Relay conf

----

## Jobs
  * Read Confs -> get time interfall I
  * Exec remaster <Conf> for I

----

## Web

show/edit #Configs

show (+live) #Logs

start/stop/plan #Jobs

start/stop/edit #PXE menue
  * default
  * all / single / none
  * live view new clients
